import Head from 'next/head'
import { useState, useEffect } from 'react'
import groupBy from 'lodash/groupBy'
import { useRouter } from 'next/router'
import keys from 'lodash/keys'
import isEmpty from 'lodash/isEmpty'
import filter from 'lodash/filter'
import Layout from '../components/Layout'
import Toggle from '../components/Toggle'
import GenreCard from '../components/GenreCard'
import { getMatchLevel } from './api/match'

async function getMatch(user1, user2) {
  return fetch(`/api/match?user1=${user1}&user2=${user2}`)
}

async function checkMatch(user1, user2) {
  try {
    const userName1 = user1.split('@')[0]
    const userName2 = user2.split('@')[0]
    const response = await getMatch(userName1, userName2)
    const data = await response.json()

    return {
      ...data,
      genres: groupBy(data?.matchLevel?.tracks, ({ albums }) => albums[0].genre),
    }
  } catch (error) {
    console.error(error)
    return {
      error,
    }
  }
}

export default function Home({ serverMatch = {}, serverUser1 = '', serverUser2 = '' }) {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [match, setMatch] = useState(serverMatch)
  const [user1, setUser1] = useState(serverUser1)
  const [user2, setUser2] = useState(serverUser2)
  const [activeUser, setActiveUser] = useState('')

  useEffect(() => {
    if (user1 && user2) {
      router.replace({ query: { user1, user2 } })
    }
  }, [user1, user2])

  function handleActiveUser(value) {
    setActiveUser(value)
  }

  function handleUser1Input(e) {
    setUser1(e.target.value)
  }

  function handleUser2Input(e) {
    setUser2(e.target.value)
  }

  async function handleCheckPlaylists() {
    setIsLoading(true)
    const match = await checkMatch(user1, user2)
    setMatch(match)
    setIsLoading(false)
  }

  return (
    <Layout>
      <Head>
        <title>🎵 Playlistner - Сравнение любимых песен</title>
        <meta
          name="description"
          content="Тут вы можете сравнить любимые песни у пользователей Яндекс.Музыки и открыть для себя новый звук"
        />
        {match?.matchLevel ? (
          <>
            <meta property="og:title" content={`🎵 Playlistner - Сравнение любимых песен ${user1} и ${user2}`} />
            <meta
              property="og:description"
              content={`У ${user1} и ${user2} любимые песни совпадают на ${match?.matchLevel?.percent}%, они ${match?.matchLevel?.compatibility} в мире музыки`}
            />
          </>
        ) : (
          <>
            <meta property="og:title" content="🎵 Playlistner - Сравнение любимых песен" />
            <meta
              property="og:description"
              content="Тут вы можете сравнить любимые песни у пользователей Яндекс.Музыки и открыть для себя новый звук"
            />
          </>
        )}
        <meta property="og:url" content="https://playlistner.ru/playlistner.jpg" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://playlistner.ru/" />
        <link rel="icon" href="/favicon.svg" />

        <script
          dangerouslySetInnerHTML={{
            __html: `
              (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
          m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
          (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

          ym(84418645, "init", {
          clickmap:true,
          trackLinks:true,
          accurateTrackBounce:true,
          webvisor:true
        });
       `,
          }}
        />
        <noscript>
          <div>
            <img src="https://mc.yandex.ru/watch/84418645" style={{ position: 'absolute', left: '-9999px' }} alt="" />
          </div>
        </noscript>
      </Head>

      <main>
        <div className="container mx-auto lg:mt-10 mt-10 p-4 lg:px-56">
          <p className="xl:text-3xl text-xl font-medium text-gray-500 xl:leading-10 leading-8 mb-6 text-center text-transparent bg-clip-text bg-gradient-to-br from-purple-400 to-indigo-600">
            Просто введите имена пользователей c Яндекс.Музыки, чтобы сравнить любимые песни
          </p>
          <form
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                e.preventDefault()
                handleCheckPlaylists()
              }
            }}
            onSubmit={(e) => {
              e.preventDefault()
              e.stopPropagation()
              handleCheckPlaylists()
            }}
          >
            <div className="grid lg:grid-cols-3 grid-cols-1 items-center justify-center text-center">
              <div className="">
                <input
                  disabled={isLoading}
                  placeholder="Имя первого пользователя"
                  className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-purple-200 focus:ring-opacity-50"
                  onChange={handleUser1Input}
                  value={user1}
                  type="text"
                  name="user1"
                />
              </div>
              <div className="text-5xl p-2">
                {isLoading ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="animate-bounce h-8 w-8 inline-flex"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3"
                    />
                  </svg>
                ) : (
                  'vs'
                )}
              </div>
              <div>
                <input
                  disabled={isLoading}
                  placeholder="Имя второго пользователя"
                  className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-300 focus:ring focus:ring-purple-200 focus:ring-opacity-50"
                  onChange={handleUser2Input}
                  value={user2}
                  type="text"
                  name="user2"
                />
              </div>
            </div>
            <div className="">
              <button
                type="submit"
                disabled={isLoading}
                className="mt-8 block w-full bg-purple-500 text-xl uppercase text-white hover:bg-purple-600 p-4 rounded-xl"
              >
                Сравнить
              </button>
            </div>
          </form>
          {match?.message && (
            <div className="mt-4 p-4 bg-red-100 rounded border-2 border-red-400">{match?.message}</div>
          )}
          {match?.matchLevel && (
            <div className="flex gap-4 mt-10 text-2xl border-2 p-4 rounded border-gray-200">
              <div>
                <div className="text-gray-400 mb-1">Музыкальная совместимость</div>
                <div className="text-purple-600 font-semibold">{match?.matchLevel?.compatibility} </div>
              </div>
              <div className="text-transparent bg-clip-text bg-gradient-to-br from-purple-400 to-indigo-600 flex-grow self-center text-right xl:text-6xl text-4xl font-semibold">
                {match?.matchLevel?.percent}%
              </div>
            </div>
          )}
        </div>
        {!isEmpty(match?.genres) && (
          <div className="container mx-auto my-8 p-4">
            <div className="flex p-6 flex-wrap items-center">
              <h3 className="xl:text-3xl text-2xl mr-4 font-extrabold tracking-tight uppercase text-transparent bg-clip-text bg-gradient-to-br from-purple-400 to-indigo-600">
                Пересечение вкусов
              </h3>
              <div className="xl:mt-px">
                <Toggle onClick={handleActiveUser} user1={user1} user2={user2} value={activeUser} />
              </div>
            </div>
            {keys(match?.genres).map((genre) => (
              <GenreCard
                key={genre}
                genre={genre}
                tracks={filter(
                  match?.genres[genre],
                  ({ ownerLogin, match }) => ownerLogin === activeUser || match || activeUser === '',
                )}
              />
            ))}
          </div>
        )}
      </main>
    </Layout>
  )
}

export async function getServerSideProps({ query: { user1, user2 } }) {
  if (user1 && user2) {
    try {
      const userName1 = user1.split('@')[0]
      const userName2 = user2.split('@')[0]
      const data = await getMatchLevel(userName1, userName2)
      const serverMatch = {
        ...data,
        genres: groupBy(data?.matchLevel?.tracks, ({ albums }) => albums[0].genre),
      }
      return {
        props: { serverMatch, serverUser1: user1, serverUser2: user2 },
      }
    } catch (error) {
      return {
        props: {
          serverUser1: user1,
          serverUser2: user2,
          serverMatch: { message: 'Плейлисты не найдены, проверьте имена пользователей' },
        },
      }
    }
  }

  return { props: {} }
}
