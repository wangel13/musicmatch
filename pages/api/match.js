import uniqBy from 'lodash/uniqBy'
import map from 'lodash/map'
import findIndex from 'lodash/findIndex'
import intersectionBy from 'lodash/intersectionBy'

async function getPlaylist(owner, kind = 3) {
  return fetch(`https://music.yandex.ru/handlers/playlist.jsx?owner=${owner}&kinds=${kind}`)
}

function checkTracksMatch(
  { tracks: tracks1Raw, owner: { login: login1 } },
  { tracks: tracks2Raw, owner: { login: login2 } },
) {
  const tracks1 = map(tracks1Raw, (track) => ({ ...track, ownerLogin: login1 }))
  const tracks2 = map(tracks2Raw, (track) => ({ ...track, ownerLogin: login2 }))

  const matches = intersectionBy(tracks1, tracks2, 'realId')

  let allTracks = uniqBy([...tracks1, ...tracks2], 'realId')
  allTracks = map(allTracks, (track) => {
    if (findIndex(matches, ({ realId }) => realId === track.realId) !== -1) {
      return { ...track, match: true }
    } else {
      return { ...track, match: false }
    }
  })
  const uniqSongs = allTracks.length - matches.length

  const percent = ((matches.length / (uniqSongs === 0 ? matches.length : uniqSongs)) * 100).toFixed(2)

  let compatibility = ''
  if (percent < 10) {
    compatibility = 'Параллельные прямые'
  } else if (percent < 20) {
    compatibility = 'Разные вселенные'
  } else if (percent < 30) {
    compatibility = 'Случайные пассажиры'
  } else if (percent < 40) {
    compatibility = 'Меломаны'
  } else if (percent < 50) {
    compatibility = 'В одном потоке'
  } else if (percent < 60) {
    compatibility = 'На одной волне'
  } else if (percent < 70) {
    compatibility = 'Неслучайная встреча'
  } else if (percent < 80) {
    compatibility = 'Совпадение? Не думаю!'
  } else if (percent < 90) {
    compatibility = 'Родственные души'
  } else if (percent <= 100) {
    compatibility = 'Единое целое'
  }

  return {
    percent,
    compatibility,
    tracks: allTracks,
  }
}

export async function getMatchLevel(user1, user2) {
  const response1 = await getPlaylist(user1)
  const response2 = await getPlaylist(user2)

  if (!response1.ok || !response2.ok) {
    throw new Error('Ответ сети был не ok.')
  }

  const { playlist: playlist1 } = await response1.json()
  const { playlist: playlist2 } = await response2.json()

  const level = checkTracksMatch(playlist1, playlist2)
  return {
    matchLevel: level,
  }
}

export default async function matchHandler(req, res) {
  const {
    query: { user1, user2 },
    method,
  } = req

  switch (method) {
    case 'GET':
      try {
        const data = await getMatchLevel(user1, user2)
        res.status(200).json(data)
      } catch (error) {
        res.status(404).json({ message: 'Плейлисты не найдены, проверьте имена пользователей' })
      }
      break
    default:
      res.setHeader('Allow', ['GET'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
