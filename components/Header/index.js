import Link from 'next/link'

function Header() {
  return (
    <header className="text-gray-600 body-font border-b border-gray-200">
      <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <Link href="/">
          <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-10 h-10 text-white p-2 bg-purple-500 rounded-xl"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3"
              />
            </svg>
            <span className="ml-3 text-xl uppercase">Playlistner</span>
          </a>
        </Link>
        <nav className="md:mr-auto md:ml-4 md:py-1 md:pl-4 md:border-l md:border-gray-400	flex flex-wrap items-center text-base justify-center">
          <Link href="/">
            <a className="mr-5 hover:text-gray-900">Сравнение любимых песен</a>
          </Link>
          {/*<Link href="/about">*/}
          {/*  <a className="mr-5 hover:text-gray-900">О проекте</a>*/}
          {/*</Link>*/}
        </nav>
        <span className="text-gray-300">© 2021 Playlistner</span>
        {/*<button className="inline-flex items-center bg-gray-100 border-0 py-1 px-3 focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">*/}
        {/*  Войти*/}
        {/*  <svg*/}
        {/*    fill="none"*/}
        {/*    stroke="currentColor"*/}
        {/*    strokeLinecap="round"*/}
        {/*    strokeLinejoin="round"*/}
        {/*    strokeWidth="2"*/}
        {/*    className="w-4 h-4 ml-1"*/}
        {/*    viewBox="0 0 24 24"*/}
        {/*  >*/}
        {/*    <path d="M5 12h14M12 5l7 7-7 7" />*/}
        {/*  </svg>*/}
        {/*</button>*/}
      </div>
    </header>
  )
}

export default Header
