import { useState } from 'react'
import filter from 'lodash/filter'
import TrackCard from '../TrackCard'
import { GENRES } from '../../helpers/genres'

function GenreCard({ tracks, genre }) {
  const [open, setOpen] = useState(false)

  function toggleOpen(e) {
    e.stopPropagation()
    setOpen(!open)
  }

  const matches = filter(tracks, ({ match }) => match)

  return (
    <div key={genre} className="border rounded border-gray-200 shadow mb-4 hover:shadow-sm">
      <div className={`${open && 'pb-0'} flex p-4 hover:cursor-pointer`} onClick={toggleOpen}>
        <button
          onClick={toggleOpen}
          className={`hover:text-purple-600 p-2 transition-transform duration-200 ease-in-out inline transform ${
            open && 'rotate-90'
          }`}
        >
          <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
            <path
              fillRule="evenodd"
              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <a
          onClick={(e) => e.stopPropagation()}
          className="p-2 text-2xl block hover:text-purple-600"
          href={`https://music.yandex.ru/genre/${genre}`}
          target="_blank"
          rel="noreferrer"
        >
          {GENRES.titles[genre] || genre}
        </a>
        <div className="p-2 text-2xl">
          <span className="bg-gradient-to-br from-green-400 to-green-600 text-transparent font-extrabold bg-clip-text">
            {matches.length}
          </span>
          <span className="font-extrabold text-gray-400">/{tracks.length}</span>
        </div>
      </div>
      {open && (
        <div className="pt-0 p-4">
          {tracks.map((track) => (
            <TrackCard key={track.id} track={track} />
          ))}
        </div>
      )}
    </div>
  )
}

export default GenreCard
