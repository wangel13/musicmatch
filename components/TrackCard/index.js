import replace from 'lodash/replace'
import Image from 'next/image'

function TrackCard({ track: { title, albums, id, match, coverUri, artists, ownerLogin } }) {
  const imgSrc = `https://${replace(coverUri, '%%', '50x50')}`
  return (
    <div className="transition-border rounded duration-200 ease-in-out flex items-center gap-4 p-2 mt-2 border-2 border-transparent hover:border-purple-500">
      {match ? (
        <span title="В обоих плейлистах">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-10 h-10 text-white p-2 bg-gradient-to-br from-green-400 to-green-600 rounded-xl"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3"
            />
          </svg>
        </span>
      ) : (
        <span title="Присутствует только в одном плейлисте">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-10 h-10 text-white p-2 bg-gradient-to-br from-red-400 to-red-600 rounded-xl"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M5.586 15H4a1 1 0 01-1-1v-4a1 1 0 011-1h1.586l4.707-4.707C10.923 3.663 12 4.109 12 5v14c0 .891-1.077 1.337-1.707.707L5.586 15z"
              clipRule="evenodd"
            />
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M17 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2"
            />
          </svg>
        </span>
      )}

      <a
        style={{ width: '50px', height: '50px' }}
        className="block relative self-center flex-shrink-0"
        href={`https://music.yandex.ru/album/${albums[0].id}/track/${id}`}
        rel="noreferrer"
        target="_blank"
      >
        <Image
          layout="fill"
          objectFit="cover"
          className="rounded"
          placeholder="blur"
          blurDataURL="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOssXpbDwAFAQIk5PtssAAAAABJRU5ErkJggg=="
          src={imgSrc}
          alt={title}
        />
      </a>
      <div>
        <a
          href={`https://music.yandex.ru/album/${albums[0].id}/track/${id}`}
          rel="noreferrer"
          className="block hover:text-purple-600"
          target="_blank"
        >
          {title}{' '}
          {artists.map(({ name }, index) => (
            <span className="text-sm text-gray-400" key={`artist_${index}`}>
              {(index ? ', ' : '') + name}
            </span>
          ))}
        </a>
        <div className="text-sm pt-1 text-gray-400">
          <span title={`из плейлиста пользователя ${ownerLogin}`}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-4 w-4 inline mr-1"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"
              />
            </svg>
            {match ? 'у каждого пользователя' : ownerLogin}
          </span>
        </div>
      </div>
    </div>
  )
}

export default TrackCard
