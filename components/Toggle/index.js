function Toggle({ onClick, value = false, user1, user2 }) {
  const button1 = value === user1 ? 'bg-green-400' : 'bg-gray-300'
  const button2 = value === user2 ? 'bg-green-400' : 'bg-gray-300'
  const button3 = value === '' ? 'bg-green-400' : 'bg-gray-300'

  return (
    <div className="rounded">
      <button
        title={`показать пересечение и треки пользователя ${user1}`}
        onClick={() => onClick(user1)}
        className={`${button1} transition-colors duration-200 ease-in-out rounded-l py-1 px-2 text-sm text-white uppercase font-semibold m-px`}
      >
        {user1 || 'имя первого пользователя'}
      </button>
      <button
        title='показать все треки'
        onClick={() => onClick('')}
        className={`${button3} transition-colors duration-200 ease-in-out py-1 px-2 text-sm text-white uppercase font-semibold m-px`}
      >
        все треки
      </button>
      <button
        title={`показать пересечение и треки пользователя ${user1}`}
        onClick={() => onClick(user2)}
        className={`${button2} transition-colors duration-200 ease-in-out rounded-r py-1 px-2 text-sm text-white uppercase font-semibold m-px`}
      >
        {user2 || 'имя второго пользователя'}
      </button>
    </div>
  )
}

export default Toggle
